import React, { Component } from "react";
import './style.css'
import { Link} from 'react-router-dom'

class NotFound extends Component {
  render() {
    return (
      <body>
        <div id="notfound">
          <div class="notfound">
            <div class="notfound-404">
              <h1>
                4<span>0</span>4
              </h1>
            </div>
            {/* <div>
              The page you are looking for might have been removed had its name
              changed or is temporarily unavailable.
            </div> */}
            <br/><br/><br/><br/><br/>
            <div>
            <Link to="#">home page</Link>
            </div>
          </div>
        </div>
      </body>
    );
  }
}

export default NotFound;

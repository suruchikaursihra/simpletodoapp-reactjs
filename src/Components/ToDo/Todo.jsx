import React, { Component } from "react";
import { withRouter } from "react-router-dom";

class Todo extends Component {
  logout = () => {
    return this.props.history.push("/");
  };

  render() {
    return (
      <div className="todoListMain">
        <form onSubmit={this.props.addItem}>
          <div className="form-group row">
            <label className="col-sm-2 col-form-label">Tasks </label>
            <div className="col-sm-10">
              <input
                className="form-control"
                name="itemText"
                placeholder="Tasks"
                value={this.props.currentItem.text}
                onChange={this.props.handleInput}
              />
            </div>
          </div>
          <div className="form-group row">
            <div className="col-sm-10">
              <button type="submit" className="btn btn-primary">
                Add Tasks
              </button>
            </div>
            <button
              className="btn btn-primary float-right"
              onClick={this.logout}
              type="button"
            >
              Logout
            </button>
          </div>
        </form>
      </div>
    );
  }
}

export default withRouter(Todo);

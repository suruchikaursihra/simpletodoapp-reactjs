import React, { Component } from "react";
import '../../../src/App.css'

class TodoList extends Component {
  constructor(props){
    super(props)
    this.state ={
      updatevalue:this.props.updateValue.text
      ,updateKey:''
    }
  }

  createTasks = item => { 
    return (
      <div key={item.key}>
      <div>
        <div className="card text-white bg-dark mb-3 " >
          <li className="list-group-item list-group-item-action list-group-item-dark"
            key={item.key}>{item.text}
            <button type="submit" className="btn btn-primary float-right" 
              data-toggle="modal" data-target="#myModal" onClick={()=>this.props.editForm(item.key)}>Edit</button>
            <button type="submit" className="btn btn-primary float-right" 
                  onClick={() => this.props.deleteItem(item.key)}>Delete</button>
          </li>
        </div>
      </div>
      <div className="modal fade " id="myModal">
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content bg-dark">
              <div className="modal-header">
                <h4 className="modal-title text-white">Edit Task</h4>
                <button type="button" className="close" data-dismiss="modal">
                  &times;
              </button>
              </div>
              <div className="modal-body text-white">
              <form onSubmit={this.props.updateItem} >
                    <div className="form-group row">
                        <label className="col-sm-2 col-form-label">Tasks </label>
                        <div className="col-sm-10">
                            <input className="form-control" name="updateValue"
                                placeholder="Tasks"
                                value={this.props.updateValue.text}
                                onChange={this.props.handleUpdateInput} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <div className="col-sm-10">
                            <button type="submit" className="btn btn-primary">Save</button>
                        </div>
                        <button type="button" className="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </form>
              </div>
            </div>
        </div>
      </div>
      </div>
    );
  };

  render() {
    const todoEntries = this.props.entries;
    const listItems = todoEntries.map(this.createTasks);
    return <ul className="list-group">{listItems}</ul>;
  }
}

export default TodoList;

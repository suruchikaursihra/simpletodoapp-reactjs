import React, { Component } from "react";

class Email extends Component {

  constructor(){
    super();
    this.state={
      validationMessage:'',
    }
  }
  
  render() {
    return (
      <div>
        <label>Email address</label>
        <div className="form-label-group">
          <input
            className="form-control"
            id={this.props.name}
            name={this.props.name}
            type={this.props.type}
            value={this.props.value}
            onChange={this.props.handleChange}
            placeholder={this.props.placeholder}
            autoFocus 
          />
        </div>
      </div>
    );
  }
}

export default Email;

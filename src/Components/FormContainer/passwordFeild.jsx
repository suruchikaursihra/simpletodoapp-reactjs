import React, { Component } from 'react';

class Password extends Component {
  render() {
    return (
      <div>
         <label>
        Password
        </label>
        <div className="form-label-group">
          <input
            className="form-control"
            id={this.props.name}
            name={this.props.name}
            type={this.props.type}
            value={this.props.value}
            onChange={this.props.handleChange}
            placeholder={this.props.placeholder}
          />
        </div>
      </div>
    );
  }
}

export default Password;

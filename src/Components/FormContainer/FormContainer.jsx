import React, { Component } from "react";
import Email from "./emailFeild";
import Password from "./passwordFeild";
import { withRouter } from "react-router-dom";

class FormContainer extends Component {
  constructor() {
    super();
    this.state = {
      newUser: {
        email: "",
        password: ""
      },
      validatedform: false,
      validationMessage: ""
    };
  }

  handleEmail = e => {
    let value = e.target.value;
    this.setState(prevState => ({
      newUser: { ...prevState.newUser, email: value }
    }));
  };

  handlePassword = e => {
    let value = e.target.value;
    this.setState(prevState => ({
      newUser: { ...prevState.newUser, password: value }
    }));
  };

  handleFormSubmit = e => {
    e.preventDefault();
    const validUser = {
      status: "success",
      responseCode: 200,
      responseMessage: "Success",
      data: {
        email: "suruchi@gmail.com",
        password: "suruchikaur"
      }
    };
    const emailValidationMessage = this.validationEmailMessage();
    const passwordVlaidationMessage = this.validationPasswordMessage();
    if (emailValidationMessage && passwordVlaidationMessage === true) {
      if (
        this.state.newUser.email === validUser.data.email &&
        this.state.newUser.password === validUser.data.password
      ) {
        return this.props.history.push("/tasks");
      } else {
        this.setState({
          validatedform: "Not a valid user"
        });
      }
    } else {
      this.setState({
        validatedform: "please fill details correctly!!!"
      });
    }
  };

  validationEmailMessage = () => {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!re.test(this.state.newUser.email)) {
      return "not a valid email";
    } else {
      return true;
    }
  };

  validationPasswordMessage = () => {
    if (this.state.newUser.password.length === 0) {
      return "password is required";
    }
    if (this.state.newUser.password.length < 8) {
      return "password cannot be less than 8 characters";
    } else {
      return true;
    }
  };

  render() {
    const emailValidationMessage = this.validationEmailMessage();
    const passwordVlaidationMessage = this.validationPasswordMessage();
    return (
      <div>
        <form className="form-signin" onSubmit={this.handleFormSubmit}>
          <Email
            type={"text"}
            title={"email"}
            name={"email"}
            value={this.state.newUser.email}
            placeholder={"Email address"}
            handleChange={this.handleEmail}
          />
          {this.state.newUser.email && (
            <div className="text text-danger" role="alert">
              {emailValidationMessage}
            </div>
          )}
          <Password
            type={"text"}
            title={"password"}
            name={"password"}
            value={this.state.newUser.password}
            placeholder={"Password"}
            handleChange={this.handlePassword}
          />
          {this.state.newUser.password && (
            <div className="text text-danger" role="alert">
              {passwordVlaidationMessage}
            </div>
          )}
          <button className="btn btn-success btn-lg btn-block " type="submit">
            Sign up
          </button>
        </form>
        <br />
        {this.state.validatedform && (
          <div className="alert alert-danger" role="alert">
            {this.state.validatedform}
          </div>
        )}
      </div>
    );
  }
}
export default withRouter(FormContainer);

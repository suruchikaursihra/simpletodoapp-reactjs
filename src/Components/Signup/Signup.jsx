import React, { Component } from "react";
import "./signup.css";
import FormContainer from "../FormContainer/FormContainer";

class Signup extends Component {
  signupForm = () => {
    return (
      <div className="container">
        <div className="row">
          <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div className="card card-signin my-5">
              <div className="card-body">
                <h5 className="card-title text-center">Sign In</h5>
                <FormContainer />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  render() {
    const signupForm = this.signupForm();
    return <div>{signupForm}</div>;
  }
}

export default Signup;

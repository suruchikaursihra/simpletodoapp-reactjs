import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import Signup from './Components/Signup/Signup'
import { Route, BrowserRouter, Switch } from 'react-router-dom'
import NotFound from './Components/NotFound/NotFound'

const routing = (
    <BrowserRouter>
            <Switch>
                <Route exact path="/" component={Signup}></Route>
                <Route path="/tasks" component={App}></Route>
                <Route component={NotFound}></Route>
            </Switch>
    </BrowserRouter>
);

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

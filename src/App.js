import React, { Component } from "react";
import Todo from "./Components/ToDo/Todo";
import "./App.css";
import TodoList from "./Components/TodoList/TodoList";
class App extends Component {
  constructor() {
    super();
    this.state = {
      updateKey: "",
      items: [],
      currentItem: { text: "", key: "" },
      updateValue:{text:''}
    };
  }
  //handle input for form
  handleInput = e => {
    const itemText = e.target.value;
    const currentItem = { text: itemText, key: Date.now() };
    this.setState({
      currentItem
    });
  };
  //handle input for edit item
  handleUpdateInput = e => {
    const updateItem = e.target.value;
    const updateValue = { text: updateItem};
    this.setState({
      updateValue
    });
  };
  //update item on form submit
  updateItem = e => {
    e.preventDefault();
    const value = this.state.items.find(items => items.key === this.state.updateKey);
    value.text = this.state.updateValue.text;
    console.log(this.state.items)
    this.setState({
      items: this.state.items,
    })
  };
  //on click Edit button save update key
  editForm = key => {
    this.setState({
      updateKey: key
    });
  };
  //add new task on form submit 
  addItem = e => {
    e.preventDefault();
    const newItem = this.state.currentItem;
    if (newItem.text !== "") {
      const items = [...this.state.items, newItem];
      this.setState({
        items: items,
        currentItem: { text: "", key: "" }
      });
    }
  };      
  //delete task
  deleteItem = key => {
    const filerValue = this.state.items.find(items => items.key === key);
    this.state.items.splice(this.state.items.indexOf(filerValue), 1);
    this.setState({
      items: this.state.items
    });
  };

  render() {
    return (
      <div>
        <div className="p-3 mb-2 bg-dark text-white">
          <Todo
            addItem={this.addItem}
            handleInput={this.handleInput}
            currentItem={this.state.currentItem}
          />
          <TodoList
            entries={this.state.items}
            handleUpdateInput={this.handleUpdateInput}
            updateItem={this.updateItem}
            editForm={this.editForm}
            deleteItem={this.deleteItem}
            updateValue={this.state.updateValue}
          />
        </div>
      </div>
    );
  }
}

export default App;
